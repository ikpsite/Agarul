$( "body" ).not( "div.HeaderDropdown" ).click( function( event ){
  $("div.HeaderDropdown.open").removeClass("open");
});

$( "div.HeaderDropdown" ).click(function( event ) {
  event.stopPropagation();
  $( "div.HeaderDropdown.open" ).not( this ).removeClass( "open" );
});

$( "div.HeaderDropdown h3").click(function( event ) {
  $( this ).parent().toggleClass( "open" );
} );

$( "div.AgarulDropdown h3").click(function( event ) {
  $( this ).parent().toggleClass( "open" );
} );

$( "#searchInput").focus(function( event ) {
  $( "#AgarulHeader-left").addClass("search-focus");
} );

$( "#searchInput").focusout(function( event ) {
  $( "#AgarulHeader-left").removeClass("search-focus");
} );
