<?php

/**
 * BaseTemplate class for Agarul skin
 *
 * @ingroup Skins
 */
class AgarulTemplate extends BaseTemplate {
	/**
	 * Outputs the entire contents of the page
	 */
	protected $AgarulWatch = '';
	protected $AgarulActions = array();
	protected $agarulPageTools = array();

	public function execute() {
		// Initialization

		$agarulPageTools = $this->data['content_navigation'];

		$agarulWatch = $this->getSkin()->getUser()->isWatched( $this->getSkin()->getRelevantTitle() )
			? 'unwatch'
			: 'watch';

		if ( isset( $agarulPageTools['actions'][$agarulWatch] ) ) {
			$agarulPageTools['views'][$agarulWatch] = $agarulPageTools['actions'][$agarulWatch];
			unset($agarulPageTools['actions'][$agarulWatch]);
		}

		$this->agarulPageTools = $agarulPageTools;

		// Skin Output
		$this->html( 'headelement' );
    require 'inc/Agarul.code.inc';
		$this->printTrail(); ?>
		</body>
		</html><?php
	}

	protected function AgarulNav($navmode){

		switch($navmode){
			case 'namespaces':
			case 'views':
			case 'variants':
			case 'actions':
				foreach ( $this->agarulPageTools[$navmode] as $key => $tab ) {
						echo $this->makeListItem( $key, $tab );
				}
				break;
			case 'personal':
				foreach ( $this->getPersonalTools() as $key => $item ) {
					echo $this->makeListItem( $key, $item );
				}
			break;
		}
	}

	protected function AgarulPageNav($nav, $navtype = 'tabs'){

		switch($navtype){
			default:
			case 'tabs':
				$navclass = 'AgarulTabs';
			break;
			case 'menu':
				$navclass = 'AgarulDropdown';
			break;
			case 'dropdown':
				$navclass = 'HeaderDropdown';
			break;
		}

		if (empty($this->agarulPageTools[$nav])) {
			$navclass .= ' emptyNav';
		}
		$nav2 = $nav;
		if ($nav == 'actions'){
			$nav2 = 'cactions';
		}
		$e_nav = htmlspecialchars($nav2);
		?>
		<div id="p-<?= $e_nav ?>" class="<?= htmlspecialchars($navclass) ?>" role="navigation">
				<h3 id="p-<?= $e_nav ?>-label"><?php $this->msg( $nav ) ?></h3>
				<ul<?php $this->html( 'userlangattributes' ) ?>>
					<?php $this->AgarulNav( $nav ); ?>
				</ul>
		</div><?php
	}

	protected function AgarulPersonal(){
		$usericon = '<i class="fas fa-lg fa-sign-in-alt"></i>';
		if ( $this->getSkin()->getUser()->isLoggedIn() ){
			$usericon = '<i class="fas fa-lg fa-user"></i>';
		}
		?>
		<div id="p-personal" class="HeaderDropdown icon-only">
			<h3 title="<?php $this->msg( 'personaltools' ) ?>"><span>
				<?= $usericon ?>
				<span class="sr-only"><?php $this->msg( 'personaltools' ) ?></span>
			</span></h3>
			<ul>
				<?php $this->AgarulNav('personal'); ?>
	 		</ul>
	 </div>
	<?php }

	protected function AgarulFooterLinks(){
		foreach ( $this->getFooterLinks() as $category => $links ) {
			echo "<ul id=\"footer-$category\">" ;
			foreach ( $links as $key ) {
				echo "<li id=\"footer-$category-$key\">";
				$this->html( $key );
				echo '</li>';
			}
			echo "</ul>" ;
		}
	}

	protected function AgarulToolBox(){
		$toolbox = $this->getToolbox();

		$toolclass = "HeaderDropdown icon-only";

		if (empty($toolbox)){
			$toolclass .= " emptyNav";
		}

		?>
		<div id="p-tb" class="<?= $toolclass ?>" aria-labelledby="p-tb-label" >
			<h3 id="p-tb-label" title="<?php $this->msg( 'toolbox' ); ?>"><span>
				<i aria-hidden class="fas fa-lg fa-cog"></i>
				<span class="sr-only"><?php $this->msg( 'toolbox' ); ?></span>
			</span></h3>
			<ul>
				<?php
				foreach ( $toolbox as $key => $tbitem ) {
					echo $this->makeListItem( $key, $tbitem );
				}
				wfRunHooks( 'SkinTemplateToolboxEnd', array( &$this ) );
				?>
		 </ul>
	 </div><?php
  }

	protected function AgarulSidebar(){
	  foreach ( $this->getSidebar(['toolbox' => false]) as $boxName => $box ) { ?>
		<div class="HeaderDropdown" id="<?= Sanitizer::escapeId( $box['id'] ) ?>"<?= Linker::tooltip( $box['id'] ) ?> >
			<h3><span><?= htmlspecialchars( $box['header'] ); ?></span></h3>
			<?php if ( is_array( $box['content'] ) ) { ?>
			<ul>
				<?php foreach ( $box['content'] as $key => $item ) {
					echo $this->makeListItem( $key, $item );
				} ?>
			</ul>
			<?php } else {
				echo $box['content'];
			} ?>
		</div>
		<?php }
	}

	protected function AgarulPageTitle(){
		if ( $this->data['title'] != '' ) {
			echo "<h1 id=\"firstHeading\" class=\"firstHeading\">";
			$this->html( 'title' );
			echo "</h1>";
		}
	}

	protected function AgarulFooterIcons(){
		foreach ( $this->getFooterIcons( 'icononly' ) as $blockName => $footerIcons ) {
			echo "<li>";
				foreach ( $footerIcons as $icon ) {
					echo $this->getSkin()->makeFooterIcon( $icon );
				 }
			echo "</li>";
		}
	}
	
}
