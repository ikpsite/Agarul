<?php
/**
 * Skin file for skin Agarul.
 *
 * @file
 * @ingroup Skins
 */

/**
 * SkinTemplate class for Agarul skin
 * @ingroup Skins
 */
class SkinAgarul extends SkinTemplate {
	var $skinname = 'agarul', $stylename = 'Agarul',
		$template = 'AgarulTemplate', $useHeadElement = true;

	/**
	 * This function adds JavaScript via ResourceLoader
	 *
	 * Use this function if your skin has a JS file(s).
	 * Otherwise you won't need this function and you can safely delete it.
	 *
	 * @param OutputPage $out
	 */

	public function initPage( OutputPage $out ) {
		parent::initPage( $out );
		$out->addModules( 'skins.agarul.js' );
	}

	/**
	 * Add CSS via ResourceLoader
	 *
	 * @param $out OutputPage
	 */
	function setupSkinUserCss( OutputPage $out ) {
		parent::setupSkinUserCss( $out );
		$out->addModuleStyles( array(
			'mediawiki.skinning.interface', 'skins.agarul.styles'
		) );
		$out->addStyle('https://use.fontawesome.com/releases/v5.0.6/css/all.css', 'screen');
	}
}
