<div id="AgarulPageWrapper">
<header id="AgarulHeader" role="header">
 <nav id="AgarulHeaderNav">
   <div id="AgarulHeader-left" class="AgarulHeader-column" role="navigation">
     <div id="AgarulSiteName">
      <a href="<?= htmlspecialchars( $this->data['nav_urls']['mainpage']['href'] ); ?>"
        <?= Xml::expandAttributes( Linker::tooltipAndAccesskeyAttribs( 'p-logo' ) ) ?>>
        <img alt="<?php $this->text( 'sitename' ); ?>" src="<?= htmlspecialchars($this->config->get( 'AgarulLogoPath' ) ) ?>" >
      </a>
     </div>
     <div id="skip-to-content" class="mw-jump">Skip to: <a href="#content">content</a>, <a href="#p-search">search</a></div>
     <?php $this->AgarulSidebar(); ?>
     <div id="p-search">
       <h3><label for="searchInput">Search</label></h3>
       <form action="<?php $this->text( 'wgScript' ); ?>">
         <input type="hidden" name="title" value="<?php $this->text( 'searchtitle' ) ?>" />
          <div id="AgarulSearchContainer">
            <?= $this->makeSearchInput( array( 'id' => 'searchInput', 'type' => 'text', 'class' => 'AgarulFormControl') ); ?>
            <i class="fas fa-search"></i>
            <?= $this->makeSearchButton('fulltext', array( 'id' => 'mw-searchButton',
            'class' => 'searchButton mw-fallbackSearchButton') ); ?>
            <?= $this->makeSearchButton('go', array( 'id' => 'searchGoButton', 'class' => 'searchButton') ); ?>
          </div>
      </form>
    </div>
   </div>
   <div id="AgarulHeader-right" class="AgarulHeader-column">
    <?php $this->AgarulToolbox(); ?>
    <?php $this->AgarulPersonal(); ?>
  </div>
 </nav>
</header>
<div id="page-header"></div>
<div id="AgarulPage">
   <nav id="pagenav" role="navigation">
     <div id="navigation-left">
       <?php $this->AgarulPageNav('namespaces'); ?>
       <?php $this->AgarulPageNav('variants', 'menu'); ?>
     </div>
     <div id="navigation-right">
    <?php $this->AgarulPageNav('views'); ?>
    <?php $this->AgarulPageNav('actions', 'menu'); ?>
    </div>
    <div style="clear:both"></div>
  </nav>
  <main id="content" class="mw-body" role="main">
      <?php echo $this->getIndicators(); ?>
      <?php $this->AgarulPageTitle(); ?>
      <div id="bodyContent" class="mw-body-content">
        <?php
        if ( $this->data['isarticle'] ) {
           echo '<div id="siteSub">';
           $this->msg( 'tagline' );
           echo '</div>';
        }
        echo '<div id="contentSub">';
        $this->html( 'subtitle' );
        echo '</div>';
        if ( $this->data['undelete'] ) {
          echo '<div id="contentSub2">';
          $this->html( 'undelete' );
          echo '</div>';
        } ?>
        <?php if ( $this->data['newtalk'] ) {
          echo '<div class="usermessage">';
          $this->html( 'newtalk' );
          echo '</div>';
        } ?>
       <?php $this->html( 'bodytext' ); ?>
       <?php $this->html( 'dataAfterContent' ); ?>
       <?php $this->html( 'catlinks' ); ?>
      <div class="visualClear"></div>
    </div>
  </main>
</div>
<footer>
<div id="footer-info-links">
  <?php $this->AgarulFooterLinks(); ?>
</div>
<ul id="footer-icons" class="noprint">
<?php $this->AgarulFooterIcons(); ?>
</ul>
  <div style="clear: both;"></div>
</footer>
<div id="AgarulOverlay"></div>
</div>
